//
// LNT - Default Prefs
//

var prefs = {
	engineOn: true,
	engineCategory: 1
};

//
// LNT - Stings & Targets
//

// Define Target String
var targetStrings = [
	/(President\SElect\s?)?(Mr\.?\s?)?(Donald\s?)?(John\s?)?(J\.?\s?)?Trump(\Ss)?/ig
];

// Define Replacement Strings
var replacementOptions = [
	{
		name: 'Putin\'s Puppet',
		url: 'https://www.washingtonpost.com/blogs/right-turn/wp/2017/01/01/trump-gives-critics-ammunition-is-he-putins-puppet/?utm_term=.9dd7799aebab'
	},
	{
		name: 'The Loser-in-Chief',
		url: 'http://www.rollingstone.com/politics/features/donald-trump-loser-in-chief-w453504'
	},
	{
		name: 'The Bully-in-Chief',
		url: 'http://www.nytimes.com/interactive/2016/01/28/upshot/donald-trump-twitter-insults.html?_r=1'
	},
	{
		name: 'The Entertainer-in-Chief',
		url: 'http://www.nydailynews.com/news/politics/chris-christie-called-trump-entertainer-in-chief-article-1.2544961'
	},
	{
		name: 'The Misogynist-in-Chief',
		url: 'http://www.wsj.com/articles/dozens-of-retired-military-brass-decry-donald-trump-1476784982'
	},
	{
		name: 'The Racist-in-Chief',
		url: 'http://www.theroot.com/articles/politics/2016/12/donald-trump-10-most-racist-things-hes-done-so-far/'
	},
	{
		name: 'The Commander-In-Tweet',
		url: 'http://www.npr.org/2016/11/18/502306687/commander-in-tweet-trumps-social-media-use-and-presidential-media-avoidance'
	},
	{
		name: 'The Liar-in-Chief',
		url: 'https://www.washingtonpost.com/blogs/plum-line/wp/2017/01/02/yes-donald-trump-lies-a-lot-and-news-organizations-should-say-so/?utm_term=.d5864f769296'
	},
	{
		name: 'Donald',
		url: 'http://www.nydailynews.com/news/national/king-call-donald-trump-mr-president-article-1.2910783'
	}
];

// Define Prefix Exceptions
exceptionsPrefix = [
	'never',
	'melania',
	'ivanka',
	'eric',
	'barron',
	'ivana',
	'tiffany',
	'fred',
	'robert',
	'mary',
	'macleod',
	'frederick',
	'john',
	'g.',
	'elizabeth',
	'tristan',
	'milos',
	'johannas',
	'chloe',
	'sophia',
	'david',
	'desmond',
	'kai',
	'madison',
	'mayanne'
];

// Define Suffix Exceptions
exceptionsSuffix = [
	'jr',
	'jr.',
	'organization',
	'foundation',
	'model',
	'steaks',
	'hotel',
	'restaurant',
	'golf',
	'tower',
	'vodka',
	'winery',
	'air',
	'tv'
];

//
// LNT - Helper Functions
//

// Funciton to Loop through each Element
// Pass back to callback
var forEach = function (array, callback, scope) {
	for (var i = 0; i < array.length; i++) {
		callback.call(array[i], i, array[i]);
	}
};

// Function to get All Indexes of an item in array
function getAllIndexes(arr, val) {
	var indexes = [],
			i;
	for(i = 0; i < arr.length; i++) {
		if (arr[i] === val){
			indexes.push(i);
		}
	}
	return indexes;
};

//
// LNT - Begin Plugin Inject
//

// Get Replacement for Instance
var replacementMaxLength = replacementOptions.length;
var replacementIndex = Math.floor(Math.random()*replacementMaxLength);
var replacementStringText = replacementOptions[replacementIndex].name;
var replacementStringUrl = replacementOptions[replacementIndex].url;
var replacementString;

// Init Function
function init(){
	// Get All Elements
	var allElements = document.querySelectorAll('*');

	// Loop through each Element
	forEach(allElements, function (index, value) {
		// If bottom-level element:
		// console.log(this.tagName);
		// console.log(this.children.length);
		if(this.children.length < 1 || this.tagName.toString().toLowerCase() == 'p'){
			// Define Variables
			var el = this;
			el.innerHTML = el.innerHTML.replace(/&nbsp;/g, ' ');
			var oldHTML = el.textContent;
			var splitContent = oldHTML.toLowerCase().split(' ');
			var hasException = false;
			var newHTML;
			// Define Replacement String
			if(el.tagName.toString().toLowerCase() == 'a' || el.tagName.toString().toLowerCase() == 'title') {
				// If is link or title tag, leave a string
				replacementString = replacementStringText;
			} else {
				// Otherwise wrap in link
				replacementString = '<a href="'+ replacementStringUrl +'" target="_blank">'+ replacementStringText +'</a>'
			}
			// Does string have Trump
			if(splitContent.indexOf('trump') > -1) {
				var trumpIndex = getAllIndexes(splitContent, 'trump');
				// Loop through instance of Trump
				for(var x = 0; x < trumpIndex.length; x++){
					var prefixNumber = parseInt(trumpIndex[x]) - 1;
					var suffixNumber = parseInt(trumpIndex[x]) + 1;
					if(prefixNumber > -1) {
						// Check Prefix for Exclusions
						if(exceptionsPrefix.indexOf(splitContent[prefixNumber].toLowerCase()) > -1){
							hasException = true;
						}
					}
					if(suffixNumber < splitContent.length){
						// Check Suffix for Exclusions
						if(exceptionsSuffix.indexOf(splitContent[suffixNumber].toLowerCase()) > -1){
							hasException = true;
						}
					}
				}
				if(hasException == false){
					// Loop through Target Strings
					for(var i = 0; i < targetStrings.length; i++) {
						var targetString = targetStrings[i];
						// Find & Replace String
						newHTML = oldHTML.replace(targetString, replacementString);
						// Replace HTML with new HTML
						this.innerHTML = newHTML;
					}
				}
			}
		}
	});
}

//
// LNT - Get Prefs
//

chrome.extension.sendMessage({action: 'prefs'}, function(response) {
	// Get stored preferences
	if(response !== undefined && response !== null) {
			prefs = response;
	} else {
		// Store Default Prefs
		localStorage.prefs = JSON.stringify(prefs);
	}
	// Check for document readyState
	var readyStateCheckInterval = setInterval(function() {
		if (document.readyState === "complete") {
			// Clear Interval
			clearInterval(readyStateCheckInterval);

			// On Load
			if(prefs.engineOn == true) {
				init();
			}

		}
	}, 10);
});

//
// LNT - Look for AJAX
//

// On AJAX Response
var _send = XMLHttpRequest.prototype.send;
XMLHttpRequest.prototype.send = function() {
	/* Wrap onreadystaechange callback */
	var callback = this.onreadystatechange;
	this.onreadystatechange = function() {
	 if (this.readyState == 4) {
		 console.log('readystate fire');
		 init();
	 }
	 callback.apply(this, arguments);
	}
	_send.apply(this, arguments);
}
