// Get Stored Preferences
chrome.extension.onMessage.addListener(
  function(request, sender, sendResponse) {
    // Get Preferences
    if(request.action === 'prefs') {
      var prefsString = localStorage.prefs;
      if(prefsString === undefined) {
        sendResponse(undefined);
      } else {
        sendResponse(JSON.parse(prefsString));
      }
    }
  });
