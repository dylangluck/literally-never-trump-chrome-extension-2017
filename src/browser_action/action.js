// Defaults
var prefs = {
  engineOn: true,
  engineCategory: 1
};

// Save Prefs
function savePrefs() {
  // Save prefs object to localstorage
  localStorage.prefs = JSON.stringify(prefs);
  // Refresh all Tabs
  chrome.tabs.getAllInWindow(null, function(tabs) {
    for(var i = 0; i < tabs.length; i++) {
      chrome.tabs.update(tabs[i].id, {url: tabs[i].url});
    }
  });
};

// On Load
function init() {

  // Set Active States - On/Off
  if(prefs.engineOn == true) {
    document.getElementById('lnt-on').classList.add('active');
  } else {
    document.getElementById('lnt-off').classList.add('active');
  }

  // Set Active States - Category
  if(prefs.engineCategory == 1) {
    document.getElementById('lnt-category-1').classList.add('active');
  } else if (prefs.engineCategory == 2){
    document.getElementById('lnt-category-2').classList.add('active');
  }

  // Click On button
  document.getElementById('lnt-on').onclick = function(){
    prefs.engineOn = true;
    savePrefs();
    document.getElementById('lnt-on').classList.add('active');
    document.getElementById('lnt-off').classList.remove('active');
    return false;
  };

  // Click Off button
  document.getElementById('lnt-off').onclick = function(){
    prefs.engineOn = false;
    savePrefs();
    document.getElementById('lnt-on').classList.remove('active');
    document.getElementById('lnt-off').classList.add('active');
    return false;
  };

  // Click Category 1 button
  document.getElementById('lnt-category-1').onclick = function(){
    prefs.engineCategory = 1;
    savePrefs();
    document.getElementById('lnt-category-1').classList.add('active');
    document.getElementById('lnt-category-2').classList.remove('active');
    return false;
  };

  // Click Category 2 button
  document.getElementById('lnt-category-2').onclick = function(){
    prefs.engineCategory = 2;
    savePrefs();
    document.getElementById('lnt-category-1').classList.remove('active');
    document.getElementById('lnt-category-2').classList.add('active');
    return false;
  };

};


// Get Preferences
chrome.extension.sendMessage({action: 'prefs'}, function(response) {
  // Get stored preferences
  if(response !== undefined && response !== null) {
      prefs = response;
  } else {
    // Store Default Prefs
    localStorage.prefs = JSON.stringify(prefs);
  }
  // Check for document readyState
  var readyStateCheckInterval = setInterval(function() {
    if (document.readyState === "complete") {
      // Clear Interval
      clearInterval(readyStateCheckInterval);

      // On Load
      init();

    }
  }, 10);
});
